package LAND.Singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Connection connection;
    private static String DATABASE_URL = "jdbc:mysql://localhost:3306/wr_logs";
    private static String USERNAME = "root";
    private static String PASSWORD = "";

    private DatabaseConnection(){

    }

    public static Connection getConnection(){
        if(connection == null){
            try{
                connection = DriverManager.getConnection(DATABASE_URL, USERNAME, PASSWORD);
            } catch (SQLException e){
                e.printStackTrace();
            }
        }
        return connection;
    }
}
