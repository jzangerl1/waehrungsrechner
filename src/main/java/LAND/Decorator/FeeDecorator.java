package LAND.Decorator;

import LAND.WR.WR;

public class FeeDecorator extends WRDecorator {
    private double fee;

    public FeeDecorator(WR wr, double fee){
        super(wr);
        this.fee=fee;
    }

    @Override
    public double umrechnen(String variante, double betrag) {

        betrag = betrag*(1.0-this.fee);
        System.out.println("Die Gebühren betragen "+(this.fee*100)+"%");
        return super.umrechnen(variante, betrag);
    }
}
