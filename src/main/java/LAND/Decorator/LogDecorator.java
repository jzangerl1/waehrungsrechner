package LAND.Decorator;

import LAND.WR.WR;

public class LogDecorator extends WRDecorator {

    public LogDecorator(WR wr) {
        super(wr);
    }

    @Override
    public double umrechnen(String variante, double betrag) {

        System.out.println("Umrechnungsvorgang, "+variante+" mit dem Betrag "+betrag);

        return super.umrechnen(variante, betrag);
    }
}
