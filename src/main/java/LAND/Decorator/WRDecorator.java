package LAND.Decorator;

import LAND.WR.WR;

public abstract class WRDecorator extends WR {
    private WR wr;

    public WRDecorator(WR wr){
        this.wr = wr;
    }

    @Override
    public double umrechnen(String variante, double betrag){
        return this.wr.umrechnen(variante, betrag);
    };

    @Override
    public String getVariante() {
        return this.wr.getVariante();
    }

    @Override
    public double getKurs() {
        return this.wr.getKurs();
    }
}
