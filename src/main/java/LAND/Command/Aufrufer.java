package LAND.Command;


public class Aufrufer {
    private ICommand command;


    public Aufrufer (ICommand command){
        this.command = command;

    }

    public void execute(){
        command.execute();
        System.out.println(command.getResult());
    }

    public void undo(){
        command.undo();
        System.out.println(command.getResult());
    }

    public void redo(){
        command.redo();
        System.out.println(command.getResult());
    }

}
