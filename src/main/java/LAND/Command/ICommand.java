package LAND.Command;

public interface ICommand {
    public void execute();
    public void undo();
    public void redo();
    public double getResult();
}
