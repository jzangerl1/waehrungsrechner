package LAND.Command;

import LAND.DAO.Logeintrag;
import LAND.DAO.LogeintragDAO;
import LAND.WR.WR;
import sun.rmi.runtime.Log;

import java.util.Stack;

public class Command implements ICommand {
    private String variante;
    private double betrag;
    private double result;
    private WR chain;
    private LogeintragDAO logeintragDAO;

    public Command(WR chain, String variante, double betrag){
        this.chain = chain;
        this.betrag = betrag;
        this.variante = variante;
        logeintragDAO = new LogeintragDAO();
    }

    @Override
    public void execute() {
        result = this.chain.umrechnen(this.variante, this.betrag);
        Logeintrag log = new Logeintrag(this.variante, this.betrag, this.result);
        logeintragDAO.save(log);
    }

    @Override
    public void undo() {
        result = this.betrag;
    }

    @Override
    public void redo() {
        this.execute();
    }

    public double getResult(){
        return this.result;
    }
}
