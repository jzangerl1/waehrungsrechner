package LAND.Adapter;

import LAND.WR.WR;

public class Sammelumrechnung implements ISammelumrechnung{
    private WR chain;

    public Sammelumrechnung(WR chain){
        this.chain = chain;
    }

    @Override
    public double sammelumrechnen(double[] betraege, String variante) {
        double summe = 0;
        for(double betrag : betraege){
            summe += chain.umrechnen(variante, betrag);
        }
        return summe;
    }
}
