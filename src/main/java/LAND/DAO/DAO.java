package LAND.DAO;

import java.util.ArrayList;

public interface DAO {

    Logeintrag getLogById(int id);

    ArrayList<Logeintrag> getAll();

    void save(Logeintrag logeintrag);

    void update(Logeintrag logeintrag);

    void delete(Logeintrag logeintrag);
}
