package LAND.DAO;

import LAND.Singleton.DatabaseConnection;
import sun.rmi.runtime.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LogeintragDAO implements DAO {

    @Override
    public Logeintrag getLogById(int id) {
        Logeintrag log = null;
        try {
            Connection myConn = DatabaseConnection.getConnection();

            PreparedStatement preparedStatement = myConn.prepareStatement("SELECT * FROM logs WHERE log_id = ?");
            preparedStatement.setInt(1, id);
            ResultSet myRs = preparedStatement.executeQuery();
            log = new Logeintrag(myRs.getInt("log_id"), myRs.getString("log_date"), myRs.getString("log_variante"), myRs.getDouble("log_betrag"), myRs.getDouble("log_umgerechneterBetrag"));
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return log;
    }

    @Override
    public ArrayList<Logeintrag> getAll() {
        ArrayList<Logeintrag> logs = new ArrayList<>();
        try {
            Connection myConn = DatabaseConnection.getConnection();

            PreparedStatement preparedStatement = myConn.prepareStatement("SELECT * FROM logs");

            ResultSet myRs = preparedStatement.executeQuery();
            while (myRs.next()) {
                Logeintrag log = new Logeintrag(myRs.getInt("log_id"), myRs.getString("log_date"), myRs.getString("log_variante"), myRs.getDouble("log_betrag"), myRs.getDouble("log_umgerechneterBetrag"));
                logs.add(log);
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return logs;
    }

    @Override
    public void save(Logeintrag logeintrag) {
        if (logeintrag != null) {
            try {
                Connection myConn = DatabaseConnection.getConnection();

                PreparedStatement preparedStatement = myConn.prepareStatement("INSERT INTO logs (log_date, log_variante, log_betrag, log_umgerechneterBetrag)VALUES(?, ?, ?, ?)");

                preparedStatement.setString(1, logeintrag.getDate());
                preparedStatement.setString(2, logeintrag.getVariante());
                preparedStatement.setDouble(3, logeintrag.getBetrag());
                preparedStatement.setDouble(4, logeintrag.getUmgerechneterBetrag());

                preparedStatement.execute();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void delete(Logeintrag logeintrag) {
        try {
            Connection myConn = DatabaseConnection.getConnection();

            PreparedStatement preparedStatement = myConn.prepareStatement("DELETE FROM logs WHERE log_id = ?");
            preparedStatement.setInt(1, logeintrag.getId());

            ResultSet myRs = preparedStatement.executeQuery();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Logeintrag logeintrag) {
        try {
            Connection myConn = DatabaseConnection.getConnection();

            PreparedStatement preparedStatement = myConn.prepareStatement("UPDATE logs SET log_date=?, log_variante=?, log_betrag=?, log_umgerechneterBetrag=? WHERE log_id=?");
            preparedStatement.setString(1, logeintrag.getDate());
            preparedStatement.setString(2, logeintrag.getVariante());
            preparedStatement.setDouble(3, logeintrag.getBetrag());
            preparedStatement.setDouble(4, logeintrag.getUmgerechneterBetrag());
            preparedStatement.setInt(5, logeintrag.getId());
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
