package LAND.DAO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logeintrag {
    private int id;
    private String date;
    private double betrag;
    private String variante;
    private double umgerechneterBetrag;

    public Logeintrag(int id, String date, String variante, double betrag, double umgerechneterBetrag){
        this.id = id;
        this.date = date;
        this.betrag = betrag;
        this.variante = variante;
        this.umgerechneterBetrag = umgerechneterBetrag;
    }

    public Logeintrag(String variante, double betrag, double umgerechneterBetrag){
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
        this.date = DATE_FORMAT.format(new Date());
        this.betrag = betrag;
        this.variante = variante;
        this.umgerechneterBetrag = umgerechneterBetrag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getBetrag() {
        return betrag;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    public String getVariante() {
        return variante;
    }

    public void setVariante(String variante) {
        this.variante = variante;
    }

    public double getUmgerechneterBetrag() { return umgerechneterBetrag; }

    public void setUmgerechneterBetrag(double umgerechneterBetrag) { this.umgerechneterBetrag = umgerechneterBetrag; }

    @Override
    public String toString() {
        return "ID "+this.id+", "+this.date+", Betrag: "+this.betrag+", Variante: "+this.variante;
    }
}
