package LAND;

import LAND.Adapter.ISammelumrechnung;
import LAND.Adapter.Sammelumrechnung;
import LAND.Command.Aufrufer;
import LAND.Command.Command;
import LAND.Command.ICommand;
import LAND.Decorator.FeeDecorator;
import LAND.Decorator.LogDecorator;
import LAND.WR.EUR2DOLLAR;
import LAND.WR.EUR2YEN;
import LAND.WR.WR;

import java.util.ArrayList;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        WR e2y = new EUR2YEN();


        e2y.addWR(new EUR2DOLLAR());
        System.out.println(e2y.printVariants());
        e2y = new FeeDecorator(e2y, 0.05);
        e2y = new LogDecorator(e2y);
        //e2y.deleteWR();
        //System.out.println(e2y.umrechnen("EUR2DOLLAR", 100));
        //System.out.println(e2y.umrechnen("EUR2YEN", 100));


        Aufrufer aufrufer = new Aufrufer(new Command(e2y, "EUR2YEN", 100));

        aufrufer.execute();

        aufrufer.undo();
        aufrufer.redo();


        //ISammelumrechnung sammelumrechnung = new Sammelumrechnung(e2y);
        //double[] betraege = {100.0, 10.0, 1000.0};
        //double result = sammelumrechnung.sammelumrechnen(betraege, "EUR2DOLLAR");
        //System.out.println("Ausgabe der Sammelumrechnung: "+result);

    }
}
