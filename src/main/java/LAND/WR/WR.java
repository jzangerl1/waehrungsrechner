package LAND.WR;

public abstract class WR implements IUmrechnen {
    protected double kurs;
    private WR nextUmrechner;

    public void setNextUmrechner(WR umrechner) {
        this.nextUmrechner = umrechner;
    }

    public WR getNextUmrechner() {
        return nextUmrechner;
    }

    public double umrechnen(String variante, double betrag) {
        double betragNeu = 0;
        //Template-Hook
        if (variante.equals(this.getVariante())) {
            if(betrag>=0) {
                betragNeu = betrag * this.getKurs();
            }
        } else {
            if (this.nextUmrechner != null) {
                betragNeu = this.nextUmrechner.umrechnen(variante, betrag);
            }
            else{
                System.out.println("Variante existiert nicht!");
            }
        }
        betragNeu= Math.round(betragNeu * 100.0) / 100.0;
        return betragNeu;
    }

    public abstract String getVariante();

    public abstract double getKurs();

    public String printVariants(){
        StringBuilder variants = new StringBuilder("Zwischen diesen  Varainten können Sie auswählen: ");
            WR wr = this;
            variants.append(this.getVariante());
            while(wr.hasNextWR()){
                wr = this.nextUmrechner;
                variants.append("/"+wr.getVariante());
            }
        return variants.toString();
    }

    private boolean hasNextWR(){
        boolean next = false;
        if(this.nextUmrechner != null){
            next = true;
        }
        return next;
    }

    public void addWR(WR wr){
        if(this.hasNextWR()){
            this.nextUmrechner.addWR(wr);
        }
        else{
            this.setNextUmrechner(wr);
        }
    }

    public void deleteWR(){
        WR prev = this;
        if(prev.hasNextWR()){
            WR next = prev.nextUmrechner;
            while(next.hasNextWR()){
                prev = next;
                next = next.nextUmrechner;
            }
        }
        prev.setNextUmrechner(null);

    }
}
