package LAND.WR;

public class EUR2YEN extends WR {
    private String variante;


    public EUR2YEN(){
        variante = "EUR2YEN";
        kurs = 124.82;
    }


    @Override
    public String getVariante() {
        return this.variante;
    }

    @Override
    public double getKurs() {
        return this.kurs;
    }
}
