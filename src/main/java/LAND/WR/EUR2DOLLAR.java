package LAND.WR;

public class EUR2DOLLAR extends WR {
    private String variante;

    public EUR2DOLLAR(){
        variante = "EUR2DOLLAR";
        kurs = 1.15;
    }

    @Override
    public String getVariante() {
        return this.variante;
    }

    @Override
    public double getKurs() {
        return this.kurs;
    }
}
