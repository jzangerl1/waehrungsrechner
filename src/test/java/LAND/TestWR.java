package LAND;

import LAND.WR.EUR2DOLLAR;
import LAND.WR.EUR2YEN;
import LAND.WR.WR;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestWR {
    private WR wr;

    @Before
    public void setUp(){
        wr = new EUR2DOLLAR();
        wr.addWR(new EUR2YEN());

    }

    @Test
    public void testAddWR(){
        wr.addWR(new EUR2YEN());
        assertNotNull(wr.getNextUmrechner());
    }

    @Test
    public void testDeleteWR(){
        wr.deleteWR();
        assertEquals(null, wr.getNextUmrechner());
    }

    @Test
    public void testUmrechnen(){
        assertEquals(115, wr.umrechnen("EUR2DOLLAR", 100.0), 0);
        assertEquals(12482, wr.umrechnen("EUR2YEN", 100.0), 0);
        assertEquals(0, wr.umrechnen("EUR2YEN", -100), 0);
    }

    @Test
    public void testVariant(){
        assertEquals("EUR2DOLLAR", wr.getVariante());
        assertEquals("EUR2YEN", wr.getNextUmrechner().getVariante());
    }

    @Test
    public void testKurs(){
        assertEquals(1.15, wr.getKurs(), 0);
        assertEquals(124.82, wr.getNextUmrechner().getKurs(), 0);
    }
}
